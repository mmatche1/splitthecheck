class CreateRestaurants < ActiveRecord::Migration[5.1]
  def change
    create_table :restaurants do |t|
      t.string :name
      t.string :address
      t.string :city
      t.string :state
      t.string :zip
      t.string :restauranttype
      t.integer :pricepoint
      t.integer :upvotes
      t.integer :downvotes

      t.timestamps
    end
  end
end
