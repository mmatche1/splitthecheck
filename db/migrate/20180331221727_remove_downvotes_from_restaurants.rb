class RemoveDownvotesFromRestaurants < ActiveRecord::Migration[5.1]
  def change
    remove_column :restaurants, :downvotes, :integer
  end
end
