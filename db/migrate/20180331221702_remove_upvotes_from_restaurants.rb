class RemoveUpvotesFromRestaurants < ActiveRecord::Migration[5.1]
  def change
    remove_column :restaurants, :upvotes, :integer
  end
end
