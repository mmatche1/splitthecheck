require 'test_helper'

class RestaurantTest < ActiveSupport::TestCase
  include Devise::Test::IntegrationHelpers
  def setup
    @nama = restaurants(:one)
    @longhorn = restaurants(:two)
  end

  test "should search restaurants with name" do
    searchResult = Restaurant.search("Nama", "")
    assert_equal @nama, searchResult[0]
  end

  test "should search restaurants with city" do
    searchResult = Restaurant.search("", "Douglasville")
    assert_equal @longhorn, searchResult[0]
  end
  
  test "should search restaurants with name and city" do
    searchResult = Restaurant.search("Nama", "Carrollton")
    assert_equal @nama, searchResult[0]
  end
  
  test "should return both when nothing matches" do
    searchResult = Restaurant.search("Nama", "Douglasville")
    
    assert_nil searchResult[0]
  end

  # nama has one downvote (from user 1)
  test "should get votes based on restaurant" do
    votes = @nama.getVotes

    assert_equal -1, votes
  end

  # user :one has downvoted nama
  test "should get votes based on user" do
    sign_in users(:one)
    user_votes = @nama.checkForUserVote(users(:one).id)
    assert_equal 1, user_votes.count
    assert_equal -1, user_votes.first.vote
  end

  # longhorn (:two) has a total of 2 votes
  # nama (:one) has a total of -1 votes
  test "should get restaurants in order of votes" do
    sortedRestaurants = Restaurant.sortByVotes
    assert_equal restaurants(:two), sortedRestaurants[0]
    assert_equal restaurants(:one), sortedRestaurants[1]
  end

  test "should get restaurants with user votes" do
    #sign_in users(:two)
    userVotedRestaurants = Restaurant.getUserVotesForAllRestaurants(users(:two).id)
    assert_equal 2, users(:two).id
    assert_equal "Longhorn", userVotedRestaurants.first.name
  end

  test "should get number of comments for given restaurant" do 
    comment_count = Restaurant.getNumberOfCommentsForRestaurant(1)
    assert_equal 2, comment_count
  end

  test "should get comments for given restaurant" do
    comments = @nama.getComments
    assert_equal "Comment 1", comments[1].user_comment
    assert_equal "Comment 2", comments[0].user_comment
  end

  test "should get restaurants with user comments for given user" do
    restaurants = Restaurant.getUserCommentedRestaurants(1)
    assert_equal "Nama", restaurants.first.name
  end
end
