require 'test_helper'

class CommentTest < ActiveSupport::TestCase
  
  test "should get user comments per restaurant" do
  	comments = Comment.getUserCommentsPerRestaurant(1, 1)
  	assert_equal 2, comments.count
  end

end
