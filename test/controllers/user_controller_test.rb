require 'test_helper'

class UserControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  test "should get show" do
  	sign_in users(:one)
    get user_show_url
    assert_response :success
  end

end
