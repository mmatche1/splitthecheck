require 'test_helper'

class RestaurantsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  setup do
    @restaurant = restaurants(:one)
  end

  test "should get index" do
    get restaurants_url
    assert_response :success
  end
  
  test "should get index with name search" do
    get restaurants_url, params: {name: "Nama"}
    assert_response :success
  end

  test "should get index with location search" do
    get restaurants_url, params: {location: "Douglasville"}
    assert_response :success
  end

  test "should get index with name and location search" do
    get restaurants_url, params: {name: "Longhorn", location: "Douglasville"}
    assert_response :success
  end

  test "should get index with incorrect search" do
    get restaurants_url, params: {name: "NotARestaurant"}
    assert_response :success
  end

  test "should show notice when no search results returned" do
    get restaurants_url, params: {name: "NotARestaurant"}
    assert_select "div.results"
  end

  test "should get new" do
    sign_in users(:one)
    get new_restaurant_url
    assert_response :success
  end

  test "should create restaurant" do
    sign_in users(:one)
    assert_difference('Restaurant.count') do
      post restaurants_url, params: { restaurant: { address: @restaurant.address, city: @restaurant.city, name: @restaurant.name, pricepoint: @restaurant.pricepoint, restauranttype: @restaurant.restauranttype, state: @restaurant.state, zip: @restaurant.zip } }
    end

    assert_redirected_to restaurant_url(Restaurant.last)
  end

  test "should show restaurant" do
    get restaurant_url(@restaurant)
    assert_response :success
  end

  test "should get edit" do
    sign_in users(:one)
    get edit_restaurant_url(@restaurant)
    assert_response :success
  end

  test "should update restaurant" do
    sign_in users(:one)
    patch restaurant_url(@restaurant), params: { restaurant: { address: @restaurant.address, city: @restaurant.city, name: @restaurant.name, pricepoint: @restaurant.pricepoint, restauranttype: @restaurant.restauranttype, state: @restaurant.state, zip: @restaurant.zip } }
    assert_redirected_to restaurant_url(@restaurant)
  end

  test "should destroy restaurant" do
    sign_in users(:one)
    assert_difference('Restaurant.count', -1) do
      delete restaurant_url(@restaurant)
    end

    assert_redirected_to restaurants_url
  end

  # fixture is set to -1 vote currently, 
  # so with upvote it evens out at 0
  test "should upvote restaurant" do
    sign_in users(:two)
    post upvote_restaurant_url(@restaurant)
    assert_equal(0, @restaurant.getVotes)
  end

  # fixture is set to -1 vote currently, 
  # so with a second downvote it will be -2
  test "should downvote restaurant" do
    sign_in users(:two)
    post downvote_restaurant_url(@restaurant)
    assert_equal(-2, @restaurant.getVotes)
  end
end
