Rails.application.routes.draw do
  
  
  resources :comments
  get 'user/show'

  get 'votes/new'

  get 'votes/create'

  get 'votes/destroy'

  resources :votes
  devise_for :users
  resources :user do
    post 'show_favorites', on: :member
    post 'show_comments', on: :member
    post 'show_votes', on: :member
  end
  resources :restaurants do
  	post 'upvote', on: :member
  	post 'downvote', on: :member
    post 'add_comment'
  end

  root 'restaurants#home', as: 'home_index'

  #root 'home#index', as: 'home_index'



  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
