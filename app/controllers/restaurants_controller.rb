class RestaurantsController < ApplicationController
  before_action :set_restaurant, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, except: [:home, :index, :show, :upvote, :downvote]
  # before_action { flash.discard }

  # GET /restaurants
  # GET /restaurants.json
  def index
    @restaurants = Restaurant.search(params[:name], params[:city]).sortByVotes
    @results = false

    if @restaurants.present?
      #@restaurants = @restaurants.order(getVotes :desc)
      @results = true
    else
      @restaurants = Restaurant.all
    end
  end

  def home
    @restaurants = Restaurant.sortByVotes
    
  end

  # GET /restaurants/1
  # GET /restaurants/1.json
  def show
    @comments = @restaurant.getComments
    @comment_user = @comments.joins("inner join users on 
                      comments.id = users.id")
  end

  # GET /restaurants/new
  def new
    @restaurant = current_user.restaurants.build
  end

  # GET /restaurants/1/edit
  def edit
  end

  # POST /restaurants
  # POST /restaurants.json
  def create
    @restaurant = current_user.restaurants.build(restaurant_params)

    respond_to do |format|
      if @restaurant.save
        format.html { redirect_to @restaurant, notice: 'Restaurant was successfully created.' }
        format.json { render :show, status: :created, location: @restaurant }
      else
        format.html { render :new }
        format.json { render json: @restaurant.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /restaurants/1
  # PATCH/PUT /restaurants/1.json
  def update
    respond_to do |format|
      if @restaurant.update(restaurant_params)
        format.html { redirect_to @restaurant, notice: 'Restaurant was successfully updated.' }
        format.json { render :show, status: :ok, location: @restaurant }
      else
        format.html { render :edit }
        format.json { render json: @restaurant.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /restaurants/1
  # DELETE /restaurants/1.json
  def destroy
    @restaurant.destroy
    respond_to do |format|
      format.html { redirect_to restaurants_url, notice: 'Restaurant was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def upvote
    if current_user
      @restaurant = Restaurant.find(params[:id])
      @user_vote = @restaurant.checkForUserVote(current_user.id)
      if @user_vote.count != 0
        @vote = @user_vote.first.vote
      end

      if @user_vote.count < 1
        @vote = Vote.create!(user_id: current_user.id, restaurant_id: @restaurant.id, vote: 1)
      elsif @user_vote.first.vote == -1
        Vote.destroy(@user_vote.first.id)
        @vote = Vote.create!(user_id: current_user.id, restaurant_id: @restaurant.id, vote: 1)
      else
        @vote = 1
        Vote.destroy(@user_vote.first.id)
      end
      respond_to do |format|
        format.html { render 'new' }
        format.js   { render layout: false }
      end
    else
      respond_to do |format|
        format.js   { flash.now[:notice] = "Please sign in to vote." }
      end
    end    
  end

  def downvote
    if current_user
      @restaurant = Restaurant.find(params[:id])
      @user_vote = @restaurant.checkForUserVote(current_user.id)
      if @user_vote.count != 0
        @vote = @user_vote.first.vote
      end

      if @user_vote.count < 1
        @vote = Vote.create!(user_id: current_user.id, restaurant_id: @restaurant.id, vote: -1)
      elsif @user_vote.first.vote == 1
        Vote.destroy(@user_vote.first.id)
        @vote = Vote.create!(user_id: current_user.id, restaurant_id: @restaurant.id, vote: -1)
      else
        @vote = -1
        Vote.destroy(@user_vote.first.id)
      end
      respond_to do |format|
        format.html { render 'new' }
        format.js   { render layout: false }
      end
    else
      respond_to do |format|
        format.js   { flash.now[:notice] = "Please sign in to vote." }
      end
    end    
  end

  def add_comment 
    @comment = Comment.new
    respond_to do |format|
      format.html { render 'new' }
      format.js   { render layout: false }
    end
  end

  #def getComments(restaurant_id)
  #  @comments = Comment.getComments(restaurant_id)
  #end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_restaurant
      @restaurant = Restaurant.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def restaurant_params
      params.require(:restaurant).permit(:name, :address, :city, :state, :zip, :restauranttype, :pricepoint, :user)
    end
end