class HomeController < ApplicationController
  def index
	@restaurants = Restaurant.order(upvotes: :desc).first(5)
  end
end
