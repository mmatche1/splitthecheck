class UserController < ApplicationController
  before_action :authenticate_user!

  def show
  end

  def show_votes
  	@restaurants = Restaurant.getUserVotesForAllRestaurants(current_user.id)
  	respond_to do |format|
      format.html { render 'new' }
      format.js   { render layout: false }
    end
  end

  def show_favorites
  	@restaurants = Restaurant.upvoted(current_user.id);
  	respond_to do |format|
      format.html { render 'new' }
      format.js   { render layout: false }
    end
  end

  def show_comments
  	@restaurants = Restaurant.getUserCommentedRestaurants(current_user.id)
  	#@comments = Comment.getUserComments(current_user.id)

  	respond_to do |format|
      format.html { render 'new' }
      format.js   { render layout: false }
    end
  end

end
