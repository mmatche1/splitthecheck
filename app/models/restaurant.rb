class Restaurant < ApplicationRecord
	belongs_to :user, optional: true
	has_many :votes
	has_many :comments

	scope :user_votes, -> {
		joins("left join votes 
			on restaurants.id = votes.restaurant_id").where("votes.id is not null")
	}
	scope :user_comments, -> (_user_id) { joins(:comments).where("comments.user_id = ?", _user_id) }
	scope :upvoted, -> (_user_id) { joins(:votes).where("votes.user_id = ? and votes.vote = 1", _user_id).sort_by(&:getVotes).reverse }

	# Search by restaurant name or city
	# or return all restaurants if none match the search
	def self.search(name, city)
		if name != "" && city != ""
			where("name like ? AND city like ?", "%#{name}%", city)
		elsif name && city == ""
			where("name like ?", "%#{name}%")
		elsif city && name == ""
			where("city = ?", city)
		elsif city == "" && city == ""
			Restaurants.all
		end
	end

	# Get total of all votes for a restaurant
	# Adding one for an upvote and subtracting one for a downvote
	def getVotes
		#@restaurant_votes = Restaurant.where("restaurant_id = ?", self.id).joins(:votes)
		@restaurant_votes = Vote.for_restaurants(self)
		@vote_total = 0
		if !@restaurant_votes.empty?
			@restaurant_votes.each do |vote|
				@vote_total = @vote_total + vote.vote
			end
		else
			@vote_total = 0
		end
		return @vote_total
	end

	# Check whether a user has voted on a restaurant
	def checkForUserVote(user_id)
		@user_vote = Vote.for_user(user_id).for_restaurants(self)
	end

	# Sort list of restaurant votes for user from higest to lowest
	def self.sortByVotes
		Restaurant.all.sort_by(&:getVotes).reverse
	end

	# Get restaurants that a given user has voted on
	# Sorted from highest to lowest
	def self.getUserVotesForAllRestaurants(user_id)
		@user_restaurants = Restaurant.user_votes.where("votes.user_id = ?", user_id).sort_by(&:getVotes).reverse
	end

	# Get count of all comments on a given restaurant
	def self.getNumberOfCommentsForRestaurant(restaurant_id)
		@comment_count = Comment.for_restaurants(restaurant_id).count
	end

	# Get all comments for a single restaurant
	def getComments
		Comment.for_restaurants(self.id)
	end

	# Get comments on all restaurants for a given user
	def self.getUserCommentedRestaurants(user_id)
		Restaurant.user_comments(user_id).uniq
	end
end
