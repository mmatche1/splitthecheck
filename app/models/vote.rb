class Vote < ApplicationRecord
	#validates :user_id, uniqueness: true#, uniqueness: {scope: :restaurant_id}
	belongs_to :user
	belongs_to :restaurant

	scope :for_restaurants, ->(_restaurant) {joins(:restaurant).where(restaurant_id: _restaurant.id)}
	scope :for_user, ->(_user_id) {where(user_id: _user_id)}
	
end
