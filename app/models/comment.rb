class Comment < ApplicationRecord
	belongs_to :restaurant
	belongs_to :user

	scope :for_restaurants, ->(_restaurant_id) {where(restaurant_id: _restaurant_id).order(created_at: :desc)}
	scope :for_user, ->(_user_id) {where(user_id: _user_id)}

	# Get all comments from a single user for a given restaurant
	def self.getUserCommentsPerRestaurant(restaurant_id, user_id)
		Comment.for_restaurants(restaurant_id).for_user(user_id)
	end
end
